# clear

## A propriedade clear especifica o que deve acontecer com o elemento que está próximo a um elemento flutuante

tags: #clear, #float, #align, #class

### fonte:[index.html](index.html)

```html

        <style>
            .clear-right {
                clear: right;
            }
        </style>

```

esta é a definição do estilo.

```html

<h4 class="clear-right">Segredo do chef</h4>

```

#### Veja o exemplo sem o estilo "clear"

![sem clear](2021-09-09-18-40-42.png)

#### Veja o exemplo com o estilo "clear"

![clear](2021-09-09-18-33-40.png)

![html com tag](2021-09-09-18-43-27.png)

![resultado](2021-09-09-18-34-43.png)

em vermelho: este é o espaço criado com o estilo "clear: right"

##### Outras fontes

[w3school/float/clear](https://www.w3schools.com/css/css_float_clear.asp)
