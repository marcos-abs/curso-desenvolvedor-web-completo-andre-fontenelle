# float

## Classe CSS que faz com que o elemento "flutue"

tags: #class, #float, #align

### fonte: [index.html](index.html)

```html
<img
            src="imagens/pato_laranja.jpg"
            width="120"
            height="120"
            title="Foto do prato Pato com Laranja"
            alt="Foto do prato Pato com Laranja"
            style="
                float: left;
                margin-right: 10px;
                border: 1px solid #666;
                margin-bottom: 10px;
            "
        />
```

#### Veja o exemplo

![Float](2021-09-09-18-07-27.png)

Em vermelho: a classe float em uso para alinha a esquerda
Em verde: a propriedade que permite a utilização do css dentro do html
Em ciano: a tag html que contém a propriedade necessária para configuração do float

##### Outras fontes

[w3school/float](https://www.w3schools.com/css/css_float.asp)
